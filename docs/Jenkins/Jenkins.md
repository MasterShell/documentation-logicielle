# Configuration matérielle minimale:

* 256 Mo de RAM
* 1 Go d'espace disque (bien que 10 Go soit un minimum recommandé si vous exécutez Jenkins en tant que conteneur Docker )
* 

# Configuration matérielle recommandée pour une petite équipe:

* 1 Go + de RAM
* 50 Go + d'espace disque


# Logiciels requis:
* Docker
* Java: voir la page des exigences Java
* Navigateur Web

# Déroulement de l'installation :
Assurez vous de bien avoir installer docker et lancer docker avec la commande "docker version" dans une invite de commande.
* Executez le fichier Jenkins.bat
* Rendez-vous sur la [page jenkins](http://localhost:8080)
* Pour avoir le mot de passe administrateur jenkins, executez les commandes suivantes dans une invite de commande : 
  * `docker exec -t -i jenkins /bin/bash`
  * `cat` + chemin que vous trouvez sur le site (de la forme suivante : cat /var/jenkins_home/secrets/initialAdminPassword)
* Copiez ensuite le mot de passe affiché dans l'invite et collez le sur le site 
* Suivez les instructions afin d'installer et de paramétrer jenkins (vous pouvez vous référer à notre doc à partir de cette étape)


# Arret des services
Vous pouvez arretez le conteneur en faisant `docker stop docker_jenkins`

Vous pouvez relancer le conteneur en faisant `docker start docker_jenkins`